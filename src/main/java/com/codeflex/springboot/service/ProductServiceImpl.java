package com.codeflex.springboot.service;

import com.codeflex.springboot.model.Product;
import com.codeflex.springboot.util.JDBCUtils;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Service("productService")
public class ProductServiceImpl implements ProductService {


    //  Using two hashmaps in order to provide performance of O(1) while fetching products
    private static HashMap<Long, Product> products = new HashMap<>();
    private static HashMap<String, Long> idNameHashMap = new HashMap<>();

    private static final String INSERT_PRODUCT_SQL = "INSERT INTO product"
            + "  (name, categoryId, price) VALUES " + " (?, ?, ?);";
    private static final String UPDATE_PRODUCT_SQL = "UPDATE product SET name=?, categoryId=?, price=? where id = ?;";
    private static final String SELECT_ALL_PRODUCTS = "SELECT * FROM product;";
    //private static final String SELECT_PRODUCT_BY_ID = "SELECT * FROM product WHERE id=?;";


    public List<Product> findAllProducts() {
        // Pagination should be added...
        products = new HashMap<>();
        try {
            Connection con = JDBCUtils.getConnection();
            Statement stmt=con.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_ALL_PRODUCTS);

            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getLong("id"));
                product.setName(rs.getString("name"));
                product.setCategoryId(rs.getInt("categoryId"));
                product.setPrice(rs.getDouble("price"));

                products.put(rs.getLong("id"), product);
//                System.out.println(rs.getInt("id"));
//                System.out.println(rs.getString("name"));
//                System.out.println(rs.getInt("categoryId"));
//                System.out.println(rs.getDouble("price"));
            }
        }catch (Exception e){
            System.out.println(e);
        }

        return new ArrayList<>(products.values());
    }

    public Product findById(long id) {

        try {
            Connection con = JDBCUtils.getConnection();
            Statement stmt=con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM product WHERE id="+id);
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getLong("id"));
                product.setName(rs.getString("name"));
                product.setCategoryId(rs.getInt("categoryId"));
                product.setPrice(rs.getDouble("price"));

                products.put(rs.getLong("id"), product);
//                System.out.println(rs.getInt("id"));
//                System.out.println(rs.getString("name"));
//                System.out.println(rs.getInt("categoryId"));
//                System.out.println(rs.getDouble("price"));
            }
        }catch (Exception e){
            System.out.println(e);
        }

        return products.get(id);
    }

    public Product findByName(String name) {

        if (idNameHashMap.get(name) != null){
            return products.get(idNameHashMap.get(name));
        }

        return null;
    }

    public void saveProduct(Product product) {
        synchronized (this) {    //  Critical section synchronized
//            System.out.println(product.getId());
//            System.out.println(product.getName());
//            System.out.println(product.getCategoryId());
//            System.out.println(product.getPrice());
//            System.out.println("--------------------");
            try (Connection connection = JDBCUtils.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PRODUCT_SQL)) {
                preparedStatement.setString(1, product.getName());
                preparedStatement.setInt(2, product.getCategoryId());
                preparedStatement.setDouble(3, product.getPrice());
                System.out.println(preparedStatement);
                preparedStatement.executeUpdate();

                System.out.println("berhasil menambahkan ke database");
            } catch (SQLException exception) {
                JDBCUtils.printSQLException(exception);
            }
//            products.put(product.getId(), product);
//            idNameHashMap.put(product.getName(), product.getId());
        }
    }

    public void updateProduct(Product product) {
        synchronized (this) {    //  Critical section synchronized

            System.out.println(product.getId());
            System.out.println(product.getName());
            System.out.println(product.getCategoryId());
            System.out.println(product.getPrice());
            System.out.println("Masuk marih....");

            try (Connection connection = JDBCUtils.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PRODUCT_SQL)) {
                preparedStatement.setString(1, product.getName());
                preparedStatement.setInt(2, product.getCategoryId());
                preparedStatement.setDouble(3, product.getPrice());
                preparedStatement.setDouble(4, product.getId());
                System.out.println(preparedStatement);
                preparedStatement.executeUpdate();

                System.out.println("berhasil update ke database");
            } catch (SQLException exception) {
                JDBCUtils.printSQLException(exception);
            }
//
//            products.put(product.getId(), product);
//            idNameHashMap.put(product.getName(), product.getId());
        }
    }

    public void deleteProductById(long id) {
        synchronized (this) {    //  Critical section synchronized
//            idNameHashMap.remove(products.get(id).getName());
//            products.remove(id);
            try {
                String sql = "DELETE FROM product WHERE id=?";
                Connection con = JDBCUtils.getConnection();
                PreparedStatement psDelete =  con.prepareStatement(sql);

                psDelete.setLong(1, id);

                psDelete.executeUpdate();
            }catch (Exception e) {
                System.out.println(e);
            }

        }
    }

    public boolean isProductExist(Product product) {
        return findByName(product.getName()) != null;
    }

    public void deleteAllProducts() {
        products.clear();
    }

}
