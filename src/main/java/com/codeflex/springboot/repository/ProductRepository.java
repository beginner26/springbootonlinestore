package com.codeflex.springboot.repository;

import com.codeflex.springboot.model.Product;

import java.util.List;

public interface ProductRepository {
    int saveProduct(Product product);
    int updateProduct(Product product);

    List<Product> findAllProducts();

    Product findById(long id);

    int deleteProductById(long id);

}
